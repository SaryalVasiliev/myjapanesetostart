//
//  KanaTableViewCell.swift
//  Japanese to Start
//
//  Created by iMac on 05.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class KanaTableViewCell: UITableViewCell {
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var Subject: UILabel!
    @IBOutlet weak var subSubject: UILabel!
    @IBOutlet weak var proccess: UILabel!
    @IBOutlet weak var info: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
