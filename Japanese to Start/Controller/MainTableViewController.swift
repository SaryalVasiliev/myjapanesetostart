//
//  MainTableViewController.swift
//  Japanese to Start
//
//  Created by iMac on 02.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    //    self.navigationController?.navigationBar.layoutIfNeeded()
        if UserDefaults.standard.bool(forKey: "used") == false {
            UserDefaults.standard.set(true, forKey: "used")
            UserDefaults.standard.set(true, forKey: "YesNo")
            UserDefaults.standard.set(10, forKey: "countQuestion")
            UserDefaults.standard.set(4, forKey: "countAnswer")
        }
    
    }
    @IBAction func clickDate(_ sender: Any) {
        let alert = UIAlertController(title: "2019年2月1日", message: "Тут будет красивое окошко с датой", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
              switch action.style{
              case .default:
                    print("default")

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")
              @unknown default:
                fatalError()
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func HiraganaClick(_ sender: Any) {
        print("Да что не так")
        self.performSegue(withIdentifier: "SelectedHK", sender: 0)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectedHK" {
            print("Ну же")
            let theDestination = (segue.destination as! SelectTableViewController)
            theDestination.subject = sender as? Int8
        }
    }
}
