//
//  TestViewController.swift
//  Japanese to Start
//
//  Created by iMac on 26.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    @IBOutlet weak var masteryProgress: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        masteryProgress.transform = masteryProgress.transform.scaledBy(x: 1, y: 20)        // Do any additional setup after loading the view.
        
        let maskLayerPath = UIBezierPath(roundedRect: masteryProgress.bounds, cornerRadius: 14.0)
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.masteryProgress.bounds
        maskLayer.path = maskLayerPath.cgPath
        masteryProgress.layer.mask = maskLayer
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
