//
//  SettingTableViewController.swift
//  Japanese to Start
//
//  Created by iMac on 01.03.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var picker:UIPickerView!
    
    var  mainLabel = ["Да/Нет", "Количество вопросов", "Количество вариантов ответа", "Автоувелечение вариантов ответа"]
    var extraLabel = ["Произношение"]
    var mainState = [""]
    var countVariation = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        mainState.append(String(UserDefaults.standard.integer(forKey: "countQuestion")))
        mainState.append(String(UserDefaults.standard.integer(forKey: "countAnswer")))
        mainState.append("")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(picker)

        picker.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        picker.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        picker.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        picker.delegate = self
        picker.dataSource = self
        picker.isHidden = true
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            if UserDefaults.standard.bool(forKey: "YesNo") {
                return 2
            }
            else {
                return 4
            }
            
        }
        else {return 1}
    }
    @IBAction func doneAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingTableViewCell
        cell.Label.text = "Да/Нет"
        if indexPath.section == 0 {
            if indexPath.row == 0 && UserDefaults.standard.bool(forKey: "YesNo"){cell.accessoryType = .checkmark}
            else {cell.accessoryType = .none}
            cell.State.text = mainState[indexPath.row]
            cell.Label.text = mainLabel[indexPath.row]
        }
        else {
            cell.Label.text = extraLabel[indexPath.row]
            cell.State.text = "Выкл."
            cell.accessoryType = .disclosureIndicator
        }
        // Configure the cell...

        return cell
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {return "основные"}
        else {return "дополнительно"}
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Урааыа")
        if  indexPath.section == 0{
            if indexPath.row == 0{
                print("Урааа")
                if UserDefaults.standard.bool(forKey: "YesNo") {
                    UserDefaults.standard.set(false, forKey: "YesNo")
                    tableView.cellForRow(at: IndexPath(row: 0, section: 0))?.accessoryType = .none
                }
                else {
                    UserDefaults.standard.set(true, forKey: "YesNo")
                    tableView.cellForRow(at: IndexPath(row: 0, section: 0))?.accessoryType = .checkmark
                }
                tableView.reloadData()
            }
            if indexPath.row == 1{
                countVariation = [5, 10, 15, 20]
                picker.reloadAllComponents()
                picker.isHidden = false
            }
            if indexPath.row == 2{
                countVariation = [4, 6, 9]
                picker.reloadAllComponents()
                picker.isHidden = false
            }
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countVariation.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(countVariation[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.isHidden = true
        if countVariation.count == 4 {
            mainState[1] = "\(countVariation[row])"
            UserDefaults.standard.set(countVariation[row], forKey: "countQuestion")
        }
        else {
            mainState[2] = "\(countVariation[row])"
            UserDefaults.standard.set(countVariation[row], forKey: "countAnswer")
        }
        
        self.tableView.reloadData()
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
