//
//  SettingTableViewCell.swift
//  Japanese to Start
//
//  Created by iMac on 01.03.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var State: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
