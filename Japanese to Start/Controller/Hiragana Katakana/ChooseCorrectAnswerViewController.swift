//
//  ChooseTranslateViewController.swift
//  Japanese to Start
//
//  Created by iMac on 06.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class ChooseCorrectAnswerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    
    
    var correctAnswer: Int?//Правильный ответ
    var incorrectAnswers = [Bool]()//Счетчик ответов пользователя
    var ask = [Int]()//Вопросы уникальные
    var step: Float!//Шаг
    var countQuestion: Int! //Количество вопросов
    var countAnswer: Int!//Количество вариантов ответа
    var YesNo: Bool!//ДаНет
    var count = 0//Счетчик прохождения
    var checkedAnswers = [String]()//Нужен для вывода table описание правильный ответов
    
    
    var showAsk1: UILabel!
    var showAsk2: UILabel!
    var showAnswer1: UIButton!
    var showAnswer2: UIButton!
    var showAnswer3: UIButton!
    var showAnswer4: UIButton!
    var showAnswer5: UIButton!
    var showAnswer6: UIButton!
    var showAnswer7: UIButton!
    var showAnswer8: UIButton!
    var showAnswer9: UIButton!
    var showAnswer10: UIButton!
    
    var repeatButton: UIButton!
    
    var incorrectAnser: UILabel!
    
    var progress: UIProgressView!
    
    @IBOutlet weak var RightBar: UIBarButtonItem!
    
    let resultTable: UITableView = {
        let tv = UITableView()
        tv.allowsSelection = false
        return tv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        RightBar.image = UIImage.init(systemName: "questionmark.circle")
        
        YesNo = UserDefaults.standard.bool(forKey: "YesNo")
        countQuestion = UserDefaults.standard.integer(forKey: "countQuestion")
        countAnswer = UserDefaults.standard.integer(forKey: "countAnswer")
        step = 1.0 / Float(countQuestion)
        
        self.title = "Тут будут очки"
        
        progress = UIProgressView()
        progress.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)!+23, width: UIScreen.main.bounds.width, height: 1)
        progress.progress = 0.0
        self.view.addSubview(progress)
        
    
        if (YesNo == true) {
            drawYesNo()
        }
        else {
            switch countAnswer {
            case 4:
                drawFourAnswer()
            case 6:
                drawSixAnswer()
            case 9:
                drawNineAnswer()
            default:
                drawYesNo()
            }
        }
        
        //Генерация вопросов
        ask = uniqueRandoms(numberOfRandoms: countQuestion, minNum: 0, maxNum: 47, blackList: nil)
        RandomizeQuize()
        

        // Do any additional setup after loading the view.
    }
    func RandomizeQuize(){
        if YesNo == true {
            if Bool.random() {
                showAsk1.text = DataBase.sharedInstance.hiragana[ask[count]].kana
                if Bool.random() {
                    correctAnswer = 1
                    showAsk2.text = DataBase.sharedInstance.hiragana[ask[count]].romaji
                }
                else {
                    correctAnswer = 0
                    showAsk2.text = DataBase.sharedInstance.hiragana[uniqueRandoms(numberOfRandoms: 1, minNum: 0, maxNum: 47, blackList: ask[count])[0]].romaji
                }
            }
            else {
                showAsk1.text = DataBase.sharedInstance.hiragana[ask[count]].romaji
                if Bool.random() {
                    correctAnswer = 1
                    showAsk2.text = DataBase.sharedInstance.hiragana[ask[count]].kana
                }
                else {
                    correctAnswer = 0
                    showAsk2.text = DataBase.sharedInstance.hiragana[uniqueRandoms(numberOfRandoms: 1, minNum: 0, maxNum: 47, blackList: ask[count])[0]].kana
                }
            }
        }
        else {
            var whereWillBeCorrectAnswer: Int!
            var inCorrectAnswerButtons = [Int]()
            var inCorrectAnswers = [Int]()
            switch countAnswer {
            case 4:
                whereWillBeCorrectAnswer = Int.random(in: 0..<4)
                inCorrectAnswerButtons = [1,2,3,4]
                inCorrectAnswerButtons.remove(at: whereWillBeCorrectAnswer)
                inCorrectAnswers = uniqueRandoms(numberOfRandoms: 3, minNum: 0, maxNum: 47, blackList: ask[count])
            case 6:
                whereWillBeCorrectAnswer = Int.random(in: 0..<6)
                inCorrectAnswerButtons = [1,2,3,4,5,6]
                inCorrectAnswerButtons.remove(at: whereWillBeCorrectAnswer)
                inCorrectAnswers = uniqueRandoms(numberOfRandoms: 5, minNum: 0, maxNum: 47, blackList: ask[count])
            case 9:
                whereWillBeCorrectAnswer = Int.random(in: 0..<9)
                inCorrectAnswerButtons = [1,2,3,4,5,6,7,8,9]
                inCorrectAnswerButtons.remove(at: whereWillBeCorrectAnswer)
                inCorrectAnswers = uniqueRandoms(numberOfRandoms: 8, minNum: 0, maxNum: 47, blackList: ask[count])
                
            default:
                break
            }
            self.correctAnswer = whereWillBeCorrectAnswer + 1
            var k = 0
            //Вопрос кана или ромадзи
            if Bool.random() {
                showAsk1.text = DataBase.sharedInstance.hiragana[ask[count]].kana
                let correctAnswerButton = self.view.viewWithTag(whereWillBeCorrectAnswer + 1) as? MyCustomButton
                correctAnswerButton?.setTitle(DataBase.sharedInstance.hiragana[ask[count]].romaji, for: .normal)
                for i in inCorrectAnswerButtons{
                    let inCorrectAnswerButton = self.view.viewWithTag(i) as? MyCustomButton
                    inCorrectAnswerButton?.setTitle(DataBase.sharedInstance.hiragana[inCorrectAnswers[k]].romaji, for: .normal)
                    k += 1
                }
            }
            else {
                showAsk1.text = DataBase.sharedInstance.hiragana[ask[count]].romaji
                let correctAnswerButton = self.view.viewWithTag(whereWillBeCorrectAnswer + 1) as? MyCustomButton
                correctAnswerButton?.setTitle(DataBase.sharedInstance.hiragana[ask[count]].kana, for: .normal)
                for i in inCorrectAnswerButtons{
                    let inCorrectAnswerButton = self.view.viewWithTag(i) as? MyCustomButton
                    inCorrectAnswerButton?.setTitle(DataBase.sharedInstance.hiragana[inCorrectAnswers[k]].kana, for: .normal)
                    k += 1
                }
            }
        }
    }
    
    
    @objc func clickAnswer(_ sender: UIButton) {
        progress.progress += step
        if correctAnswer == sender.tag {
            incorrectAnswers.append(false)
        }
        else {
            incorrectAnswers.append(true)
        }
        if count == countQuestion - 1  {
            drawResult()
            return
        }
        
        count += 1
        RandomizeQuize()
    }
    
 
    func drawYesNo() {
        showAsk1 = MyCustomLabel(frame: CGRect(x: 20.0, y: UIScreen.main.bounds.height / 2 - (UIScreen.main.bounds.width - 60.0) / 2 - 20.0, width: (UIScreen.main.bounds.width - 60.0) / 2, height: (UIScreen.main.bounds.width - 60.0) / 2))
        self.view.addSubview(showAsk1)

        showAsk2 = MyCustomLabel(frame: CGRect(x: 40.0 + (UIScreen.main.bounds.width - 60.0) / 2, y: UIScreen.main.bounds.height / 2 - (UIScreen.main.bounds.width - 60.0) / 2 - 20.0, width: (UIScreen.main.bounds.width - 60.0) / 2, height: (UIScreen.main.bounds.width - 60.0) / 2))
        self.view.addSubview(showAsk2)

        showAnswer1 = MyCustomButton(frame: CGRect(x: 20.0, y: UIScreen.main.bounds.height - 170.0, width: 100.0, height: 100.0), type: "YesNo")
        showAnswer1.tag = 0
        showAnswer1.layer.borderColor = UIColor.red.cgColor
        showAnswer1.setTitleColor(.red, for: .normal)
        showAnswer1.setTitle("нет", for: .normal)
        showAnswer1.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer1)

        showAnswer2 = MyCustomButton(frame: CGRect(x: UIScreen.main.bounds.width - 120.0, y: UIScreen.main.bounds.height - 170.0, width: 100.0, height: 100.0), type: "YesNo")
        showAnswer2.tag = 1
        showAnswer2.setTitleColor(UIColor.init(hexFromString: "#33CC66"), for: .normal)
        showAnswer2.layer.borderColor = UIColor.init(hexFromString: "#33CC66").cgColor
        showAnswer2.setTitle("да", for: .normal)
        showAnswer2.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer2)
    }
    
    func drawFourAnswer(){
        showAsk1 = MyCustomLabel(frame: CGRect(x: (UIScreen.main.bounds.width - (UIScreen.main.bounds.width - 60.0) / 2) / 2, y: UIScreen.main.bounds.height / 2 - (UIScreen.main.bounds.width - 60.0) / 2 - 20.0, width: (UIScreen.main.bounds.width - 60.0) / 2, height: (UIScreen.main.bounds.width - 60.0) / 2))
        self.view.addSubview(showAsk1)
        
        showAnswer1 = MyCustomButton(frame: CGRect(x: 20.0, y: UIScreen.main.bounds.height / 2 + 20, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 2), type: "Shadow")
        showAnswer1.tag = 1
        showAnswer1.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer1)
        
        showAnswer2 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 2 + 60.0, y: UIScreen.main.bounds.height / 2 + 20, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 2), type: "Shadow")
        showAnswer2.tag = 2
        showAnswer2.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer2)
        
        showAnswer3 = MyCustomButton(frame: CGRect(x: 20, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 2 + 60, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 2), type: "Shadow")
        showAnswer3.tag = 3
        showAnswer3.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer3)
        
        showAnswer4 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 2 + 60.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 2 + 60, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 2), type: "Shadow")
        showAnswer4.tag = 4
        showAnswer4.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer4)
    }
    
    func drawSixAnswer(){
        showAsk1 = MyCustomLabel(frame: CGRect(x: (UIScreen.main.bounds.width - (UIScreen.main.bounds.width - 60.0) / 2) / 2, y: UIScreen.main.bounds.height / 2 - (UIScreen.main.bounds.width - 60.0) / 2 - 20.0, width: (UIScreen.main.bounds.width - 60.0) / 2, height: (UIScreen.main.bounds.width - 60.0) / 2))
        self.view.addSubview(showAsk1)
        
        showAnswer1 = MyCustomButton(frame: CGRect(x: 20.0, y: UIScreen.main.bounds.height / 2 + 20, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer1.tag = 1
        showAnswer1.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer1)
        
        showAnswer2 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 2 + 60.0, y: UIScreen.main.bounds.height / 2 + 20, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer2.tag = 2
        showAnswer2.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer2)
        
        showAnswer3 = MyCustomButton(frame: CGRect(x: 20, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 + 40, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer3.tag = 3
        showAnswer3.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer3)
        
        showAnswer4 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 2 + 60.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 + 40, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer4.tag = 4
        showAnswer4.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer4)
        
        showAnswer5 = MyCustomButton(frame: CGRect(x: 20.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 * 2 + 60, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer5.tag = 5
        showAnswer5.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer5)
        
        showAnswer6 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 2 + 60.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 * 2 + 60, width: (UIScreen.main.bounds.width - 80) / 2, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer6.tag = 6
        showAnswer6.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer6)
    }
    
    func drawNineAnswer(){
        showAsk1 = MyCustomLabel(frame: CGRect(x: (UIScreen.main.bounds.width - (UIScreen.main.bounds.width - 60.0) / 2) / 2, y: UIScreen.main.bounds.height / 2 - (UIScreen.main.bounds.width - 60.0) / 2 - 20.0, width: (UIScreen.main.bounds.width - 60.0) / 2, height: (UIScreen.main.bounds.width - 60.0) / 2))
        self.view.addSubview(showAsk1)
        
        showAnswer1 = MyCustomButton(frame: CGRect(x: 20.0, y: UIScreen.main.bounds.height / 2 + 20, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer1.tag = 1
        showAnswer1.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer1)
        
        showAnswer2 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 3 + 40.0, y: UIScreen.main.bounds.height / 2 + 20, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer2.tag = 2
        showAnswer2.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer2)
        
        showAnswer3 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 3 * 2 + 60.0, y: UIScreen.main.bounds.height / 2 + 20, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer3.tag = 3
        showAnswer3.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer3)
        
        showAnswer4 = MyCustomButton(frame: CGRect(x: 20, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 + 40, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer4.tag = 4
        showAnswer4.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer4)
        
        showAnswer5 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 3 + 40.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 + 40, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer5.tag = 5
        showAnswer5.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer5)
        
        showAnswer6 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 3 * 2 + 60.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 + 40, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer6.tag = 6
        showAnswer6.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer6)
        
        showAnswer7 = MyCustomButton(frame: CGRect(x: 20.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 * 2 + 60, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer7.tag = 7
        showAnswer7.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer7)
        
        showAnswer8 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 3 + 40.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 * 2 + 60, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer8.tag = 8
        showAnswer8.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer8)
        
        showAnswer9 = MyCustomButton(frame: CGRect(x: (UIScreen.main.bounds.width - 80) / 3 * 2 + 60.0, y: UIScreen.main.bounds.height / 2 + (UIScreen.main.bounds.height / 2 - 80) / 3 * 2 + 60, width: (UIScreen.main.bounds.width - 80) / 3, height: (UIScreen.main.bounds.height / 2 - 80) / 3), type: "Shadow")
        showAnswer9.tag = 9
        showAnswer9.addTarget(nil, action: #selector(clickAnswer(_:)), for: .touchUpInside)
        self.view.addSubview(showAnswer9)
    }
    
    func drawResult(){
        checkAnswers()
        RightBar.image = UIImage.init(systemName: "gear")
        self.tabBarController?.tabBar.isHidden = false
        self.view.subviews.forEach { $0.removeFromSuperview() }//Удаление всех элементов
        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! + 10, width: UIScreen.main.bounds.width, height: 50)
        var correctSum = 0
        for answer in incorrectAnswers {
            if answer == false {correctSum += 1}
        }
        label.text = "Результат: \(correctSum)/\(countQuestion!):"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 27)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.minimumScaleFactor = 0.4
        self.view.addSubview(label)
        let label2 = UILabel()
        label2.frame = CGRect(x: 15, y: (self.navigationController?.navigationBar.frame.size.height)! + 60, width: (UIScreen.main.bounds.width / 2), height: 30)
        label2.text = "Уровень: 0"
        label2.textColor = .black
        label2.font = UIFont.systemFont(ofSize: 21)
        label2.adjustsFontSizeToFitWidth = true
        label2.textAlignment = .left
        label2.minimumScaleFactor = 0.4
        self.view.addSubview(label2)
        let label3 = UILabel()
        label3.frame = CGRect(x: (UIScreen.main.bounds.width / 2) + 10, y: (self.navigationController?.navigationBar.frame.size.height)! + 60, width: (UIScreen.main.bounds.width / 2) - 15, height: 30)
        label3.text = "+253"
        label3.textColor = .link
        label3.font = UIFont.systemFont(ofSize: 17)
        label3.adjustsFontSizeToFitWidth = true
        label3.textAlignment = .right
        label3.minimumScaleFactor = 0.4
        self.view.addSubview(label3)
        let progressLevel = UIProgressView()
        progressLevel.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! + 110, width: UIScreen.main.bounds.width, height: 1)
        progressLevel.transform = progressLevel.transform.scaledBy(x: 1, y: 20)
        progressLevel.progress = 0.75
        self.view.addSubview(progressLevel)
        resultTable.delegate = self
        resultTable.dataSource = self
        resultTable.register(BundleCell.self, forCellReuseIdentifier: "cell")
        resultTable.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.size.height)! + 110, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (self.tabBarController?.tabBar.frame.height)! - (self.navigationController?.navigationBar.frame.height)! - 164)
        view.addSubview(resultTable)
        repeatButton = UIButton(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - (self.tabBarController?.tabBar.frame.height)! - 54, width: UIScreen.main.bounds.width, height: 54))
        repeatButton.setTitle("Попробовать еще раз", for: .normal)
        repeatButton.setTitleColor(.white, for: .normal)
        repeatButton.backgroundColor = UIColor.init(hexFromString: "#3333CC")
        repeatButton.addTarget(nil, action: #selector(repeatAction), for: .touchUpInside)
        resultTable.reloadData()
        view.addSubview(repeatButton)
        

    }
    func checkAnswers(){
            for i in 0...countQuestion - 1{
                if incorrectAnswers[i] {
                    checkedAnswers.append("Правильный ответ: \(DataBase.sharedInstance.hiragana[ask[i]].kana ?? "") - \(DataBase.sharedInstance.hiragana[ask[i]].romaji ?? "")")
                }
                else {
                    checkedAnswers.append(" \(DataBase.sharedInstance.hiragana[ask[i]].kana ?? "") - \(DataBase.sharedInstance.hiragana[ask[i]].romaji ?? "")")
                }
            }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countQuestion
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = checkedAnswers[indexPath.row]
        if incorrectAnswers[indexPath.row] {
            cell.imageView?.image = UIImage.init(systemName: "xmark")
            cell.imageView?.tintColor = .red
            cell.backgroundColor = UIColor.init(red: 255, green: 0, blue: 0, alpha: 0.3)
        }
        else {
            cell.imageView?.image = UIImage.init(systemName: "chevron.down")
            cell.imageView?.tintColor = .green
            cell.backgroundColor = UIColor.init(red: 0, green: 255, blue: 0, alpha: 0.3)
        }
        return cell
    }

    @objc func repeatAction() -> Void {
        self.view.subviews.forEach { $0.removeFromSuperview() }//Удаление всех элементов
        RightBar.image = UIImage.init(systemName: "questionmark.circle")
        self.tabBarController?.tabBar.isHidden = true
        count = 0
        incorrectAnswers.removeAll()
        ask.removeAll()
        checkedAnswers.removeAll()
        YesNo = UserDefaults.standard.bool(forKey: "YesNo")
        countQuestion = UserDefaults.standard.integer(forKey: "countQuestion")
        countAnswer = UserDefaults.standard.integer(forKey: "countAnswer")
        //Генерация вопросов
        ask = uniqueRandoms(numberOfRandoms: countQuestion, minNum: 0, maxNum: 47, blackList: nil)
        progress.progress = 0.0
        self.view.addSubview(progress)
        step = 1.0 / Float(countQuestion)
        if (YesNo == true) {
            drawYesNo()
        }
        else {
            switch countAnswer {
            case 4:
                drawFourAnswer()
            case 6:
                drawSixAnswer()
            case 9:
                drawNineAnswer()
            default:
                drawYesNo()
            }
            
        }
        RandomizeQuize()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    func uniqueRandoms(numberOfRandoms: Int, minNum: Int, maxNum: UInt32, blackList: Int?) -> [Int] {
        var uniqueNumbers = Set<Int>()
        while uniqueNumbers.count < numberOfRandoms {
            uniqueNumbers.insert(Int(arc4random_uniform(maxNum + 1)) + minNum)
        }
        if let blackList = blackList {
            if uniqueNumbers.contains(blackList) {
                while uniqueNumbers.count < numberOfRandoms+1 {
                    uniqueNumbers.insert(Int(arc4random_uniform(maxNum + 1)) + minNum)
                }
                uniqueNumbers.remove(blackList)
            }
        }
        return uniqueNumbers.shuffled()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class BundleCell: UITableViewCell {
    let cellView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    func setup() {
        addSubview(cellView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
//Цвет согласно hex
extension UIColor {
    convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt64 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt64(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
