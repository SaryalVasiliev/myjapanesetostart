//
//  ShowViewController.swift
//  Japanese to Start
//
//  Created by iMac on 06.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class ShowViewController: UIViewController {

    @IBOutlet weak var Kana: UILabel!
    @IBOutlet weak var Romaji: UILabel!
    
    @IBOutlet weak var PrevButton: UIButton!
    @IBOutlet weak var NextButton: UIButton!
    
    @IBOutlet weak var eyeShow: UIBarButtonItem!
    var id = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Просмотр"
        Kana.text = DataBase.sharedInstance.hiragana[0].kana
        Romaji.text = DataBase.sharedInstance.hiragana[0].romaji
        PrevButton.isHidden = true

        // Do any additional setup after loading the view.
    }
    @IBAction func ShowAnswer(_ sender: Any){
        if Romaji.isHidden == false {
            Romaji.isHidden = true
            eyeShow.image = UIImage(systemName: "eye.fill")
        }
        else {
            Romaji.isHidden = false
            eyeShow.image = UIImage(systemName: "eye")
        }
        
    }
    @IBAction func Next(_ sender: Any) {
        id += 1
        Kana.text = DataBase.sharedInstance.hiragana[id - 1].kana
        Romaji.text = DataBase.sharedInstance.hiragana[id - 1].romaji
        if id == 48 {
            NextButton.isHidden = true
        }
        else {
            NextButton.isHidden = false
        }
        PrevButton.isHidden = false
    }
    
    @IBAction func Previous(_ sender: Any) {
        id -= 1
        Kana.text = DataBase.sharedInstance.hiragana[id - 1].kana
        Romaji.text = DataBase.sharedInstance.hiragana[id - 1].romaji
        if id == 1 {
            PrevButton.isHidden = true
        }
        else {
            PrevButton.isHidden = false
        }
        NextButton.isHidden = false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
