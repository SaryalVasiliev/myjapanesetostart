//
//  LessonViewController.swift
//  Japanese to Start
//
//  Created by iMac on 06.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class LessonViewController: UIViewController {
    var lessonNumber: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Урок №\(String(lessonNumber ?? -1))"

        // Do any additional setup after loading the view.
    }
    
    @IBAction func showCard(_ sender: Any) {
        self.performSegue(withIdentifier: "Show", sender: self)
    }
    @IBAction func chooseCard(_ sender: Any) {
        self.performSegue(withIdentifier: "Choose", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
