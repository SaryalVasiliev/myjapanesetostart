//
//  SelectTableViewController.swift
//  Japanese to Start
//
//  Created by iMac on 02.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class SelectTableViewController: UITableViewController {
    @IBOutlet weak var table: UITableView!
    var subject:Int8!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.view.backgroundColor = .darkGray
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        switch subject {
        case 0:
            self.title = "Хирагана"
        default:
            print("Придумать ошибку")
        }
        table.rowHeight = 86
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
         // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source
    
    
    @IBAction func info(_ sender: Any) {
        let alert = UIAlertController(title: "Тут будет информация о теме урока", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
              switch action.style{
              case .default:
                    print("default")

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")
              @unknown default:
                fatalError()
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 100
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! KanaTableViewCell
        cell.Subject.text = "Урок " + String(indexPath.row + 1) + " (レッスン" + String(indexPath.row + 1) + ")"
        cell.subSubject.text = "Столбцы 1, 2 (列1、2)"
        cell.selectionStyle = .none
        if indexPath.row > 20 {
            cell.proccess.text = "休業"
            cell.superView.backgroundColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
            cell.info.isEnabled = false
        }
        else {
            cell.proccess.text = "開く"
            cell.superView.backgroundColor = #colorLiteral(red: 1, green: 0.3222170472, blue: 0.6638055444, alpha: 1)
            cell.info.isEnabled = true
        }
 
        cell.indentationLevel = 54
        cell.info.tag = indexPath.row + 1
        cell.info.addTarget(self, action: #selector(clickInfo), for: .touchUpInside)
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let cell = self.tableView(self.table, cellForRowAt: indexPath) as! KanaTableViewCell
        if indexPath.row > 20 {
            
            let alert = UIAlertController(title: "Внимание", message: "Это урок вам недоступен", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                      switch action.style{
                      case .default:
                            print("default")

                      case .cancel:
                            print("cancel")

                      case .destructive:
                            print("destructive")
                      @unknown default:
                        fatalError()
                    }}))
                self.present(alert, animated: true, completion: nil)
        }
        else {
            self.performSegue(withIdentifier: "Lesson", sender: indexPath.row + 1)
        }
    }

    @objc func clickInfo(button: UIButton){
        let alert = UIAlertController(title: "Тут будет информация об уроке", message: "Об уроке №\(button.tag)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
              switch action.style{
              case .default:
                    print("default")

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")
              @unknown default:
                fatalError()
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
        if self.isMovingFromParent{
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Lesson" {
            let theDestination = (segue.destination as! LessonViewController)
            theDestination.lessonNumber = sender as? Int
        }
    }
    

    
 
    



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
