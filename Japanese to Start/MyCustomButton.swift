//
//  MyCustomButton.swift
//  Japanese to Start
//
//  Created by iMac on 20.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import UIKit

class MyCustomButton: UIButton {
    init(frame: CGRect, type: String) {
        super.init(frame: frame)
        switch type {
        case "YesNo":
            YesNoFontBorder()
        case "Shadow":
            GrayWithShadow()
        default:
            fatalError("Type not existing")
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func YesNoFontBorder() -> Void {
        self.titleLabel?.font = .systemFont(ofSize: 35.0)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.4
        self.layer.cornerRadius = 50.0
        self.layer.borderWidth = 1.0
    }
    
    func GrayWithShadow() -> Void {
        layer.cornerRadius = frame.height / 4
        clipsToBounds = true
        layer.masksToBounds = false
        layer.shadowRadius = 5
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowColor = UIColor.init(hexFromString: "#CCCCCC").cgColor
        backgroundColor = .white
        setTitleColor(.black, for: .normal)
        self.titleLabel?.font = .systemFont(ofSize: 30.0)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.4
    }
}
