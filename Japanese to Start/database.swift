	//
//  database.swift
//  Japanese to Start
//
//  Created by iMac on 06.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import Foundation
    
struct HiraganaData: Codable {
    var id: Int!
    var kana: String!
    var romaji: String!
}
class DataBase: NSObject {
    static let sharedInstance = DataBase()
    var hiragana = [HiraganaData(id: 1, kana: "あ", romaji: "а"), HiraganaData(id: 2, kana: "い", romaji: "и"), HiraganaData(id: 3, kana: "う", romaji: "у"), HiraganaData(id: 4, kana: "え", romaji: "э"), HiraganaData(id: 5, kana: "お", romaji: "о"), HiraganaData(id: 6, kana: "か", romaji: "ка"), HiraganaData(id: 7, kana: "き", romaji: "ки"), HiraganaData(id: 8, kana: "く", romaji: "ку"), HiraganaData(id: 9, kana: "け", romaji: "кэ"), HiraganaData(id: 10, kana: "こ", romaji: "ко"), HiraganaData(id: 11, kana: "さ", romaji: "са"), HiraganaData(id: 12, kana: "し", romaji: "си"), HiraganaData(id: 13, kana: "す", romaji: "су"), HiraganaData(id: 14, kana: "せ", romaji: "сэ"), HiraganaData(id: 15, kana: "そ", romaji: "со"), HiraganaData(id: 16, kana: "た", romaji: "та"), HiraganaData(id: 17, kana: "ち", romaji: "ти"), HiraganaData(id: 18, kana: "つ", romaji: "цу"), HiraganaData(id: 19, kana: "て", romaji: "тэ"), HiraganaData(id: 20, kana: "と", romaji: "то"), HiraganaData(id: 21, kana: "な", romaji: "на"), HiraganaData(id: 22, kana: "に", romaji: "ни"), HiraganaData(id: 23, kana: "ぬ", romaji: "ну"), HiraganaData(id: 24, kana: "ね", romaji: "нэ"), HiraganaData(id: 25, kana: "の", romaji: "но"), HiraganaData(id: 26, kana: "は", romaji: "ха"), HiraganaData(id: 27, kana: "ひ", romaji: "хи"), HiraganaData(id: 28, kana: "ふ", romaji: "фу"), HiraganaData(id: 29, kana: "へ", romaji: "хэ"), HiraganaData(id: 30, kana: "ほ", romaji: "хо"), HiraganaData(id: 31, kana: "ま", romaji: "ма"), HiraganaData(id: 32, kana: "み", romaji: "ми"), HiraganaData(id: 33, kana: "む", romaji: "му"), HiraganaData(id: 34, kana: "め", romaji: "мэ"), HiraganaData(id: 35, kana: "も", romaji: "мо"), HiraganaData(id: 36, kana: "や", romaji: "я"), HiraganaData(id: 37, kana: "ゆ", romaji: "ю"), HiraganaData(id: 38, kana: "よ", romaji: "ё"), HiraganaData(id: 39, kana: "ら", romaji: "ра"), HiraganaData(id: 40, kana: "り", romaji: "ри"), HiraganaData(id: 41, kana: "る", romaji: "ру"), HiraganaData(id: 42, kana: "れ", romaji: "рэ"), HiraganaData(id: 43, kana: "ろ", romaji: "ро"), HiraganaData(id: 44, kana: "わ", romaji: "ва"), HiraganaData(id: 45, kana: "ゐ", romaji: "ви"), HiraganaData(id: 46, kana: "ゑ", romaji: "вэ"), HiraganaData(id: 47, kana: "を", romaji: "во"), HiraganaData(id: 48, kana: "ん", romaji: "н")]
}
