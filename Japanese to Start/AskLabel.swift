//
//  AskLabel.swift
//  Japanese to Start
//
//  Created by iMac on 20.02.2020.
//  Copyright © 2020 East-Nord. All rights reserved.
//

import Foundation
import UIKit

class AskLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setFont()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        //super.init(coder: coder)
        //setFont()
    }
    
    func setFont() -> Void {
        font = .systemFont(ofSize: 100)
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.4
        //showAsk1.backgroundColor = .
        textAlignment = .center
    }
}
